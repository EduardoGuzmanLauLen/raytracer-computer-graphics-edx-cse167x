#include "MathMethods.h"
#include <math.h>

float MathMethods::dot(const Vector& vec_1, const Vector& vec_2)
{
	return vec_1.x * vec_2.x + vec_1.y * vec_2.y + vec_1.z * vec_2.z;
}

float MathMethods::dot(const Vector4& vec_1, const Vector4& vec_2)
{
	return vec_1.w * vec_2.w + vec_1.x * vec_2.x + vec_1.y * vec_2.y + vec_1.z * vec_2.z;
}

Vector MathMethods::cross(const Vector& vec3_1, const Vector& vec3_2)
{
	return Vector(vec3_1.y * vec3_2.z - vec3_1.z * vec3_2.y, vec3_1.z * vec3_2.x - vec3_1.x * vec3_2.z, vec3_1.x * vec3_2.y - vec3_1.y * vec3_2.x);
}

Matrix MathMethods::inverse(const Matrix& matrix)
{
	float Coef00 = matrix.mat[2][2] * matrix.mat[3][3] - matrix.mat[3][2] * matrix.mat[2][3];
	float Coef02 = matrix.mat[1][2] * matrix.mat[3][3] - matrix.mat[3][2] * matrix.mat[1][3];
	float Coef03 = matrix.mat[1][2] * matrix.mat[2][3] - matrix.mat[2][2] * matrix.mat[1][3];

	float Coef04 = matrix.mat[2][1] * matrix.mat[3][3] - matrix.mat[3][1] * matrix.mat[2][3];
	float Coef06 = matrix.mat[1][1] * matrix.mat[3][3] - matrix.mat[3][1] * matrix.mat[1][3];
	float Coef07 = matrix.mat[1][1] * matrix.mat[2][3] - matrix.mat[2][1] * matrix.mat[1][3];

	float Coef08 = matrix.mat[2][1] * matrix.mat[3][2] - matrix.mat[3][1] * matrix.mat[2][2];
	float Coef10 = matrix.mat[1][1] * matrix.mat[3][2] - matrix.mat[3][1] * matrix.mat[1][2];
	float Coef11 = matrix.mat[1][1] * matrix.mat[2][2] - matrix.mat[2][1] * matrix.mat[1][2];

	float Coef12 = matrix.mat[2][0] * matrix.mat[3][3] - matrix.mat[3][0] * matrix.mat[2][3];
	float Coef14 = matrix.mat[1][0] * matrix.mat[3][3] - matrix.mat[3][0] * matrix.mat[1][3];
	float Coef15 = matrix.mat[1][0] * matrix.mat[2][3] - matrix.mat[2][0] * matrix.mat[1][3];

	float Coef16 = matrix.mat[2][0] * matrix.mat[3][2] - matrix.mat[3][0] * matrix.mat[2][2];
	float Coef18 = matrix.mat[1][0] * matrix.mat[3][2] - matrix.mat[3][0] * matrix.mat[1][2];
	float Coef19 = matrix.mat[1][0] * matrix.mat[2][2] - matrix.mat[2][0] * matrix.mat[1][2];

	float Coef20 = matrix.mat[2][0] * matrix.mat[3][1] - matrix.mat[3][0] * matrix.mat[2][1];
	float Coef22 = matrix.mat[1][0] * matrix.mat[3][1] - matrix.mat[3][0] * matrix.mat[1][1];
	float Coef23 = matrix.mat[1][0] * matrix.mat[2][1] - matrix.mat[2][0] * matrix.mat[1][1];

	Vector4 SignA(+1, -1, +1, -1);
	Vector4 SignB(-1, +1, -1, +1);

	Vector4 Fac0(Coef00, Coef00, Coef02, Coef03);
	Vector4 Fac1(Coef04, Coef04, Coef06, Coef07);
	Vector4 Fac2(Coef08, Coef08, Coef10, Coef11);
	Vector4 Fac3(Coef12, Coef12, Coef14, Coef15);
	Vector4 Fac4(Coef16, Coef16, Coef18, Coef19);
	Vector4 Fac5(Coef20, Coef20, Coef22, Coef23);

	Vector4 Vec0(matrix.mat[1][0], matrix.mat[0][0], matrix.mat[0][0], matrix.mat[0][0]);
	Vector4 Vec1(matrix.mat[1][1], matrix.mat[0][1], matrix.mat[0][1], matrix.mat[0][1]);
	Vector4 Vec2(matrix.mat[1][2], matrix.mat[0][2], matrix.mat[0][2], matrix.mat[0][2]);
	Vector4 Vec3(matrix.mat[1][3], matrix.mat[0][3], matrix.mat[0][3], matrix.mat[0][3]);

	Vector4 Inv0 = SignA * (Vec1 * Fac0 - Vec2 * Fac1 + Vec3 * Fac2);
	Vector4 Inv1 = SignB * (Vec0 * Fac0 - Vec2 * Fac3 + Vec3 * Fac4);
	Vector4 Inv2 = SignA * (Vec0 * Fac1 - Vec1 * Fac3 + Vec3 * Fac5);
	Vector4 Inv3 = SignB * (Vec0 * Fac2 - Vec1 * Fac4 + Vec2 * Fac5);

	Matrix Inverse(Inv0.w, Inv1.w, Inv2.w, Inv3.w,
		Inv0.x, Inv1.x, Inv2.x, Inv3.x,
		Inv0.y, Inv1.y, Inv2.y, Inv3.y,
		Inv0.z, Inv1.z, Inv2.z, Inv3.z);

	Vector4 Row0(Inverse.mat[0][0], Inverse.mat[1][0], Inverse.mat[2][0], Inverse.mat[3][0]);
	float Determinant = MathMethods::dot(Vector4(matrix.mat[0][0], matrix.mat[0][1], matrix.mat[0][2], matrix.mat[0][3]), Row0);
	Inverse = Inverse / Determinant;
	return Inverse;
}

Matrix MathMethods::transpose(const Matrix& matrix)
{
	Matrix result;
	result.mat[0][0] = matrix.mat[0][0];
	result.mat[0][1] = matrix.mat[1][0];
	result.mat[0][2] = matrix.mat[2][0];
	result.mat[0][3] = matrix.mat[3][0];

	result.mat[1][0] = matrix.mat[0][1];
	result.mat[1][1] = matrix.mat[1][1];
	result.mat[1][2] = matrix.mat[2][1];
	result.mat[1][3] = matrix.mat[3][1];

	result.mat[2][0] = matrix.mat[0][2];
	result.mat[2][1] = matrix.mat[1][2];
	result.mat[2][2] = matrix.mat[2][2];
	result.mat[2][3] = matrix.mat[3][2];

	result.mat[3][0] = matrix.mat[0][3];
	result.mat[3][1] = matrix.mat[1][3];
	result.mat[3][2] = matrix.mat[2][3];
	result.mat[3][3] = matrix.mat[3][3];

	return result;
}

Matrix  MathMethods::inverseTranspose(const Matrix& matrix)
{
	return MathMethods::inverse(MathMethods::transpose(matrix));
}

float MathMethods::determinant(const Matrix& matrix)
{
	float determinant
		= matrix.mat[0][0] * matrix.mat[1][1] * matrix.mat[2][2] * matrix.mat[3][3]
		+ matrix.mat[0][0] * matrix.mat[2][1] * matrix.mat[3][2] * matrix.mat[1][3]
		+ matrix.mat[0][0] * matrix.mat[3][1] * matrix.mat[1][2] * matrix.mat[2][3]

		+ matrix.mat[1][0] * matrix.mat[0][1] * matrix.mat[3][2] * matrix.mat[2][3]
		+ matrix.mat[1][0] * matrix.mat[2][1] * matrix.mat[0][2] * matrix.mat[3][3]
		+ matrix.mat[1][0] * matrix.mat[3][1] * matrix.mat[2][2] * matrix.mat[0][3]

		+ matrix.mat[2][0] * matrix.mat[0][1] * matrix.mat[1][2] * matrix.mat[3][3]
		+ matrix.mat[2][0] * matrix.mat[1][1] * matrix.mat[3][2] * matrix.mat[0][3]
		+ matrix.mat[2][0] * matrix.mat[3][1] * matrix.mat[0][2] * matrix.mat[1][3]

		+ matrix.mat[3][0] * matrix.mat[0][1] * matrix.mat[2][2] * matrix.mat[1][3]
		+ matrix.mat[3][0] * matrix.mat[1][1] * matrix.mat[0][2] * matrix.mat[2][3]
		+ matrix.mat[3][0] * matrix.mat[2][1] * matrix.mat[1][2] * matrix.mat[0][3]

		- matrix.mat[0][0] * matrix.mat[1][1] * matrix.mat[3][2] * matrix.mat[2][3]
		- matrix.mat[0][0] * matrix.mat[2][1] * matrix.mat[1][2] * matrix.mat[3][3]
		- matrix.mat[0][0] * matrix.mat[3][1] * matrix.mat[2][2] * matrix.mat[1][3]

		- matrix.mat[1][0] * matrix.mat[0][1] * matrix.mat[2][2] * matrix.mat[3][3]
		- matrix.mat[1][0] * matrix.mat[2][1] * matrix.mat[3][2] * matrix.mat[0][3]
		- matrix.mat[1][0] * matrix.mat[3][1] * matrix.mat[0][2] * matrix.mat[2][3]

		- matrix.mat[2][0] * matrix.mat[0][1] * matrix.mat[3][2] * matrix.mat[1][3]
		- matrix.mat[2][0] * matrix.mat[1][1] * matrix.mat[0][2] * matrix.mat[3][3]
		- matrix.mat[2][0] * matrix.mat[3][1] * matrix.mat[1][2] * matrix.mat[0][3]

		- matrix.mat[3][0] * matrix.mat[0][1] * matrix.mat[1][2] * matrix.mat[2][3]
		- matrix.mat[3][0] * matrix.mat[1][1] * matrix.mat[2][2] * matrix.mat[0][3]
		- matrix.mat[3][0] * matrix.mat[2][1] * matrix.mat[0][2] * matrix.mat[1][3];
	return determinant;
}

float MathMethods::radians(const float& angle)
{
	return angle * MATH_PI / 180;
}

Vector MathMethods::oppositeDirection(const Vector& vector)
{
	return Vector(vector) * -1;
}

Matrix MathMethods::translation(float x, float y, float z)
{
	return Matrix(1, 0, 0, x,
		0, 1, 0, y,
		0, 0, 1, z,
		0, 0, 0, 1);
}

Matrix MathMethods::rotation(float x, float y, float z, float angle)
{
	Normal axis(x, y, z);
	float rad = MathMethods::radians(angle);

	float cos_angle = cos(rad);
	float sin_angle = sin(rad);

	Matrix3 identity(1.0);
	Matrix3 mat_times_transpose(
		axis.x * axis.x, axis.x * axis.y, axis.x * axis.z,
		axis.x * axis.y, axis.y * axis.y, axis.y * axis.z,
		axis.x * axis.z, axis.y * axis.z, axis.z * axis.z);
	Matrix3 conjugate_transpose(
		0, -axis.z, axis.y,
		axis.z, 0, -axis.x,
		-axis.y, axis.x, 0);

	Matrix result((identity * cos_angle) + (mat_times_transpose * (1 - cos_angle)) + (conjugate_transpose * sin_angle));
	return result;
}

Matrix MathMethods::scaling(float x, float y, float z)
{
	return Matrix(x, 0, 0, 0,
		0, y, 0, 0,
		0, 0, z, 0,
		0, 0, 0, 1);
}

float MathMethods::length(const Vector& v)
{
	return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}