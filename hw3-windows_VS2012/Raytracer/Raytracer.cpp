#include "Derived.h"
#include "MathMethods.h"
#include <algorithm>

RayTracer::RayTracer()
{
	this->maxDepth = 5;
}

RayTracer::RayTracer(int maxDepth)
{
	this->maxDepth = maxDepth;
}

void RayTracer::trace(Ray& ray, int depth, Color* color)
{
	color->r = 0.0;
	color->g = 0.0;
	color->b = 0.0;
	if (depth > this->maxDepth)
	{
		return;
	}
	float t_hit;
	float smallest_time = INFINITY;
	Intersection intersection;

	Primitive* closest_primitive = NULL;
	Intersection closest_intersection;
	int counter = 0;
	for (auto primitive = scene->primitives.begin(); primitive != scene->primitives.end(); primitive++)
	{
		counter++;
		if ((*primitive)->intersect(ray, &t_hit, &intersection))
		{
			if (t_hit < smallest_time)
			{
				smallest_time = t_hit;
				closest_primitive = *primitive;
				closest_intersection = intersection;
			}
		}
	}

	BRDF materials;
	Color light_color;
	Ray light_ray;
	Vector reflected_direction;			// used for two different things: intersection point to light source (shadows check), and opposite angle reflection across normal (for recursive reflection)
	Vector refracted_direction;			// used for refraction ray angle (maybe I should combine this with the above, or separate all)
	Ray reflected_ray;
	Ray refracted_ray;
	float n;

	if (closest_primitive)
	{
		closest_primitive->getBRDF(closest_intersection.localGeo, &materials);
		*color = *color + materials.COMPONENT_AMBIENT;
		*color = *color + materials.COMPONENT_EMISSIVITY;

		/* Then, add on the Phong shading contribution from each light source, if light not obstructed by any other object */
		for (auto light = scene->lights.begin(); light != scene->lights.end(); light++)
		{
			(*light)->generateLightRay(closest_intersection.localGeo, &light_ray, &light_color);
			bool isNotBlocked = true;
			for (auto primitive = scene->primitives.begin(); primitive != scene->primitives.end(); ++primitive)
			{
				if ((*primitive) != closest_primitive && (*primitive)->intersectP(light_ray))
				{
					isNotBlocked = false;
				}
			}

			if (isNotBlocked)
			{

				float nDotL = std::max(MathMethods::dot(closest_intersection.localGeo.normal, light_ray.direction.normalize()), 0.0f);
				Color diffuse = materials.COMPONENT_DIFFUSE * (*light)->color * nDotL;

				Vector halfvec = (light_ray.direction.normalize() + (MathMethods::oppositeDirection(ray.direction)).normalize()).normalize();
				float nDotH = std::max(MathMethods::dot(closest_intersection.localGeo.normal, halfvec), 0.0f);
				Color specular = materials.COMPONENT_SPECULAR * (*light)->color * pow(nDotH, materials.SHININESS);

				Color result = diffuse + specular;

				if (PointLight* point = dynamic_cast<PointLight*>(*light))
				{
					float r = MathMethods::length(point->position - closest_intersection.localGeo.position);
					result = result * (1.0 / (scene->attenuation[2] * r * r + scene->attenuation[1] * r + scene->attenuation[0]));
				}

				*color = (*color) + result;
			}
		}

		/* For reflective surfaces, recursively raytrace to find reflection */
		if (materials.COMPONENT_SPECULAR.r != 0.0 || materials.COMPONENT_SPECULAR.g != 0.0 || materials.COMPONENT_SPECULAR.b != 0.0)
		{
			reflected_direction = ray.direction.normalize() - (closest_intersection.localGeo.normal * (2 * MathMethods::dot(ray.direction.normalize(), closest_intersection.localGeo.normal)));
			reflected_ray.position = closest_intersection.localGeo.position;
			reflected_ray.direction = reflected_direction;
			reflected_ray.t_min = 0.001;
			reflected_ray.t_max = 1000;
			trace(reflected_ray, depth + 1, &light_color);
			*color = *color + materials.COMPONENT_SPECULAR * light_color;
		}

		/* For transparent objects, recursive raytrace to compute refraction */
		if (materials.COMPONENT_TRANSPARENCY.r != 0.0 || materials.COMPONENT_TRANSPARENCY.g != 0.0 || materials.COMPONENT_TRANSPARENCY.b != 0.0)
		{
			if (ray.direction * closest_intersection.localGeo.normal < 0)
			{
				n = 1.0 / materials.INDEX_REFRACTION;
				float c1 = ray.direction * MathMethods::oppositeDirection(closest_intersection.localGeo.normal);
				float c2 = sqrt(1 - n * n * (1 - c1 * c1));
				refracted_direction = ray.direction * n + closest_intersection.localGeo.normal * (c1 - c2) * n;
			
				refracted_ray.position = closest_intersection.localGeo.position;
				refracted_ray.direction = refracted_direction;
				refracted_ray.t_min = 0.001;
				refracted_ray.t_max = 1000;
				trace(refracted_ray, depth + 1, &light_color);
				*color = *color + materials.COMPONENT_TRANSPARENCY * light_color;
			} 
			else
			{
				n = materials.INDEX_REFRACTION;
				float c1 = ray.direction * closest_intersection.localGeo.normal;
				float c2 = sqrt(1 - n * n * (1 - c1 * c1));
				refracted_direction = ray.direction*n + closest_intersection.localGeo.normal* (c1 - c2)*n;

				refracted_ray.position = closest_intersection.localGeo.position;
				refracted_ray.direction = refracted_direction;
				refracted_ray.t_min = 0.001;
				refracted_ray.t_max = 1000;
				trace(refracted_ray, depth + 1, &light_color);
				*color = *color + materials.COMPONENT_TRANSPARENCY * light_color;
			}
		}
	}
}


