#ifndef EAGLE_RAYTRACING_IOMETHODS
#define EAGLE_RAYTRACING_IOMETHODS
#include <string>
#include <stack>
#include "Basic.h"

class IOMethods
{
public:
	static bool readValues(std::stringstream &s, const int numvals, float* values);
};

#endif