#ifndef EAGLE_RAYTRACE_BASIC
#define EAGLE_RAYTRACE_BASIC

struct Matrix3;
struct Vector
{
	float x{ 0.0 }, y{ 0.0 }, z{ 0.0 };
	Vector();
	Vector(float x, float y, float z);
	Vector operator+(const Vector& v) const;
	Vector operator-(const Vector& v) const;
	Vector cross(const Vector& v) const;
	Vector operator*(float scalar) const;
	Vector operator/(float scalar) const;
	Vector normalize() const;
	float magnitude() const;
	float dot(const Vector& v) const;
	float operator*(const Vector& v) const;
};

struct Vector4
{
	float w{ 0.0 }, x{ 0.0 }, y{ 0.0 }, z{ 0.0 };
	Vector4();
	Vector4(float w, float x, float y, float z);
	Vector4 operator+(const Vector4& v);
	Vector4 operator-(const Vector4& v);
	Vector4 operator*(float scalar);
	Vector4 operator/(float scalar);
	Vector4 normalize();
	float magnitude();
	float dot(const Vector4& v);
	Vector4 operator*(const Vector4& v);
};

struct Normal : public Vector
{
	Normal();
	Normal(float x, float y, float z);
	Normal(Vector& v);
	Normal operator+(Vector& v) const;
	Normal operator-(Vector& v) const;
};

struct Point
{
	float x{ 0.0 }, y{ 0.0 }, z{ 0.0 };
	Point();
	Point(float x, float y, float z);
	Point operator+(const Vector& v) const;
	Point operator-(const Vector& v) const;
	Vector operator+(const Point& v) const;
	Vector operator-(const Point& v) const;
};

struct Ray
{
	Point position;
	Vector direction;
	float t_min, t_max;
	Ray();
	Ray(Point position, Vector direction, float t_min, float t_max);
	Point pointAtTime(float t) const;
	void print() const;
};

struct Matrix
{
	float mat[4][4];
	Matrix();
	Matrix(float a);
	Matrix(float a, float b, float c, float d, float e, float f, float g, float h, float i, float j, float k, float l, float m, float n, float o, float p);
	Matrix(const Matrix3& m);
	Matrix operator+(const Matrix& m) const;
	Matrix operator-(const Matrix& m) const;
	Matrix operator* (const float& scalar) const;
	Matrix operator/ (const float& scalar) const;
	Matrix operator* (const Matrix& multiplier) const;
	Vector operator* (const Vector& v) const;
	Vector operator* (const Normal& n) const;
	void print() const;
};

struct Matrix3
{
	float mat[3][3];
	Matrix3();
	Matrix3(float a);
	Matrix3(float a, float b, float c, float d, float e, float f, float g, float h, float i);
	Matrix3 operator+(const Matrix3& m) const;
	Matrix3 operator-(const Matrix3& m) const;
	Matrix3 operator* (const float& scalar) const;
	Matrix3 operator/ (const float& scalar) const;
	Matrix3 operator* (const Matrix3& multiplier) const;
	Vector operator* (const Vector& v) const;
	Vector operator* (const Normal& n) const;
	void print() const;
};

struct LocalGeo{
	Point position;
	Normal normal;
	LocalGeo();
};

struct Transformation{
	Matrix matrix;
	Matrix minvt;

	Transformation();
	Transformation(const Matrix& m);
	static Transformation identityTransformation();
	Transformation inverseTranspose() const;
	Transformation inverse() const;
	Point operator* (const Point& p) const;
	Vector operator* (const Vector& v) const;
	Vector operator* (const Normal& v) const;
	Ray operator* (const Ray& r) const;
	LocalGeo operator* (const LocalGeo& lg) const;
	void setMatrix(const Matrix& m);
	void print() const;
};

struct Color
{
	float r, g, b;
	Color();
	Color(float r, float g, float b);
	Color operator+(const Color& c) const;
	Color operator-(const Color& c) const;
	Color operator*(const Color& c) const;
	Color add(const float& x, const float& y, const float& z) const;
	Color subtract(const float& x, const float& y, const float& z) const;
	Color operator*(const float& n) const;
	Color operator/(const float& n) const;
};

struct BRDF
{
	Color COMPONENT_DIFFUSE, COMPONENT_SPECULAR, COMPONENT_AMBIENT, COMPONENT_EMISSIVITY, COMPONENT_TRANSPARENCY;
	float SHININESS{ 1.0 }, INDEX_REFRACTION{ 1.0 };
	BRDF();
	BRDF(Color ambient, Color diffuse, Color specular, Color emissivity, Color transparency, float shininess, float refraction);
};

struct Sample
{
	float x, y;
	Sample();
	Sample(float x, float y);
};

#endif